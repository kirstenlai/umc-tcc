from app.main import app
from unittest import TestCase


class TestRoute(TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_main_route(self):
        response = self.app.get('/')

        self.assertEqual(200, response.status_code)
